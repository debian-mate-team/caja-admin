Source: caja-admin
Section: x11
Priority: optional
Maintainer: Debian+Ubuntu MATE Packaging Team <debian-mate@lists.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
 Vangelis Mouhtsis <vangelis@gnugr.org>,
Build-Depends:
 meson (>= 0.50.0),
 debhelper-compat (= 13),
 dh-python,
 python3:any | python3-all:any | python3-dev:any | python3-all-dev:any,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/infirit/caja-admin/
Vcs-Browser: https://salsa.debian.org/debian-mate-team/caja-admin
Vcs-Git: https://salsa.debian.org/debian-mate-team/caja-admin.git
X-Python-Version: >= 3.6

Package: caja-admin
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 python3-gi,
 gir1.2-gtk-3.0,
 caja,
 polkitd,
 pkexec,
 python3-caja (>= 1.23.0),
Recommends:
 pluma,
 mate-terminal,
Description: Add administrative actions to Caja's right-click menu
 Caja Admin is a simple Python extension for the Caja file manager that
 adds some administrative actions to the right-click menu:
 .
   - Open as Administrator: opens a folder in a new Caja window running
     with administrator (root) privileges.
   - Edit as Administrator: opens a file in a Pluma window running with
     administrator (root) privileges.
   - Run as Administrator: runs an executable file with administrator
     (root) privileges inside a MATE Terminal.
